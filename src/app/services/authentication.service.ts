import { Injectable } from '@angular/core';

import { AngularFireAuth } from '@angular/fire/auth';
import { Observable } from 'rxjs';
import * as firebase from 'firebase/app';


@Injectable({ providedIn: 'root' })
export class AuthenticationService {

  private user: Observable<firebase.User>;

  constructor(private afAuth: AngularFireAuth) {
    this.user = afAuth.authState;
  }

  login(email: string, passsword: string): Promise<firebase.auth.UserCredential> {
    return this.afAuth.auth.signInWithEmailAndPassword(email, passsword);
  }

  logout(): Promise<void> {
    return this.afAuth.auth.signOut();
  }

  resetPAssword(email: string) {
    this.afAuth.auth.sendPasswordResetEmail(email);
  }

}
