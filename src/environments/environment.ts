// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyDhcsgWGDAeexxHPH4RfwcmgecqXdN4MUI",
    authDomain: "requisicoes-app-f1b52.firebaseapp.com",
    databaseURL: "https://requisicoes-app-f1b52.firebaseio.com",
    projectId: "requisicoes-app-f1b52",
    storageBucket: "requisicoes-app-f1b52.appspot.com",
    messagingSenderId: "754950889317",
    appId: "1:754950889317:web:f2f22211aa4bcf6e504b8c",
    measurementId: "G-387P1FMVHR"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
